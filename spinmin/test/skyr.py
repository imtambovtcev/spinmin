from ase.build import bcc100
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from spinmin.utilities import random_spins, plot_xy
import numpy as np

atoms = bcc100('Fe', a=2.856, orthogonal=True, size=(20, 20, 1))
spins = random_spins(len(atoms), seed=7)
atoms.set_initial_magnetic_moments(spins)

ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': 10.0,
                                    'DM': 5.0,
                                    'Z_ad': {'ampl': 2.0,
                                             'dir': np.asarray([0.0, 0.0, 1.0])},
                                    'r_c': 1.45})

opt = UnitaryOptimisation(spins, atoms, ham)
opt.run()
plot_xy(atoms, spins)