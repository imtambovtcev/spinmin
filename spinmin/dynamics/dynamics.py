from .sib import SIB


class SpinDynamics:

    def __init__(self, hamiltonian, dyn_algo='SIB',
                 dt=10.0, alpha=0.1, eq_type='LLG',
                 temperature=None, n_steps=333,
                 damping_only=False):

        self.ham = hamiltonian
        self.dyn_algo = dyn_algo
        self.dt = dt  # fs
        self.alpha = alpha
        self.damping_only = damping_only
        self.temperature = temperature  # meV
        self.eq_type = eq_type

        self.algo = SIB(eq_type, alpha, dt, temperature, damping_only)
        self.n_steps = n_steps
        self.iters = 0

    def run(self):

        # self.energy = []
        # self.energy.append(self.ham.get_energy())
        for self.iters in range(self.n_steps):
            self.algo.update(self.ham)
            # self.energy.append()
            print(self.iters, self.ham.get_energy(), self.algo.error)
            if self.algo.error < 1.0e-4:
                print(self.iters)
                break
