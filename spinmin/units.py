EGM = 1.760859644e11  # electron gyromagnetic ration 1 / (s T)
BohrMagn = 5.7883818012e-5  # ev/T, Bohr Magneton

R = EGM / BohrMagn * 1.0e-16 * 1.0e-3  # (1 / (fs meV))