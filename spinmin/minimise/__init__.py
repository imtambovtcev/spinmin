from .searchdir import SteepestDescent, FRcg, HZcg, PRPcg, PRcg, QuickMin, LBFGS
from .linesearch import UnitStepLength, StrongWolfeConditions, Parabola


def search_direction(method):
    if isinstance(method, str):
        method = {'name': method}

    if isinstance(method, dict):
        kwargs = method.copy()
        name = kwargs.pop('name')
        if name == 'SD':
            return SteepestDescent()
        elif name == 'FRcg':
            return FRcg()
        elif name == 'HZcg':
            return HZcg()
        elif name == 'PRPcg':
            return PRPcg()
        elif name == 'PRcg':
            return PRcg()
        elif name == 'QuickMin':
            return QuickMin()
        elif name == 'LBFGS':
            return LBFGS(**kwargs)
        else:
            raise ValueError('Check keyword for search direction!')
    else:
        raise ValueError('Check keyword for search direction!')


def line_search_algorithm(method, objective_function, sda):

    if isinstance(method, str):
        method = {'name': method}

    if isinstance(method, dict):
        kwargs = method.copy()
        kwargs['sda'] = sda
        name = kwargs.pop('name')
        if name == 'UnitStep':
            return UnitStepLength(objective_function)
        elif name == 'Parabola':
            return Parabola(objective_function)
        elif name == 'SwcAwc':
            return StrongWolfeConditions(objective_function,
                                         **kwargs
                                         )
        else:
            raise ValueError('Check keyword for line search!')
    else:
        raise ValueError('Check keyword for line search!')
