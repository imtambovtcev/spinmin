import numpy as np


def skh2u(upp_tr, real=True):

    if type(upp_tr) == list:
        upp_tr = np.asarray(upp_tr)
    if np.allclose(upp_tr, np.zeros_like(upp_tr)):
        return np.eye(3)

    evals, evec = egdecomp_skhm(upp_tr)

    u_mat = (evec * np.exp(evals)) @ evec.T.conj()

    if real is True:
        u_mat = u_mat.real

    return u_mat


def egdecomp_skhm(upp_tr):

    if type(upp_tr) == list:
        upp_tr = np.asarray(upp_tr)
    if np.allclose(upp_tr, np.zeros_like(upp_tr)):
        return np.array([0.0, 0.0, 0.0]), np.eye(3)

    x = np.sqrt(np.dot(upp_tr, upp_tr))
    a = upp_tr[0]
    b = upp_tr[1]
    c = upp_tr[2]

    v1 = np.array([c/x, -b/x, a/x])
    ac = np.sqrt(a**2 + c**2)

    if ac < 1.0e-12:
        if b < 0.0:
            v2 = np.array([-1.0j, 0.0, 1.0]) / np.sqrt(2.0)
        else:
            v2 = np.array([1.0j, 0.0, 1.0]) / np.sqrt(2.0)
    else:
        v2 = np.array([b * c + 1.0j * a * x,
                       a**2 + c**2,
                       a * b - 1.0j * c * x]) / \
             (x * np.sqrt(2.0) * ac)

    v3 = v2.conj()

    return np.array([0.0, -1.0j*x, 1.0j*x]), \
           np.array([v1, v2, v3]).T


def get_grad(upp_tr, m_uptr):

    m_cc = np.zeros(shape=(3, 3))
    m_cc[([1, 2, 0], [2, 0, 1])] = m_uptr
    m_cc += -m_cc.T

    evals, evec = egdecomp_skhm(upp_tr)

    grad = np.dot(evec.T.conj(), np.dot(m_cc, evec))
    grad = grad * D_matrix(1.0j * evals)
    grad = np.dot(evec, np.dot(grad, evec.T.conj()))
    return grad[([0, 0, 1], [1, 2, 2])].real


def D_matrix(omega):

    m = omega.shape[0]
    u_m = np.ones(shape=(m, m))

    u_m = omega[:, np.newaxis] * u_m - omega * u_m

    with np.errstate(divide='ignore', invalid='ignore'):
        u_m = 1.0j * np.divide(np.exp(-1.0j * u_m) - 1.0, u_m)

    u_m[np.isnan(u_m)] = 1.0
    u_m[np.isinf(u_m)] = 1.0

    return u_m


def random_spins(n_atoms, length=1.0, seed=None):

    if seed is not None:
        assert type(seed) == int
        np.random.seed(seed)
    spins = np.zeros(shape=(n_atoms, 3))
    for i, s in enumerate(spins):
        z = np.random.uniform(-1.0, 1.0)
        phi = np.random.uniform(0.0, 2.0 * np.pi)
        x = np.sqrt(1.0 - z ** 2) * np.cos(phi)
        y = np.sqrt(1.0 - z ** 2) * np.sin(phi)
        spins[i] = length * np.array([x, y, z])
    return spins


def plot_xy(atoms, spins, name='Figure'):

    import matplotlib.pyplot as plt

    pos = atoms.positions
    plt.figure(figsize=(8, 6))
    plt.axis('equal')
    plt.xlabel(r'x coordinate ($\AA$)')
    plt.ylabel(r'y coordinate ($\AA$)')
    plt.quiver(pos[:, 0], pos[:, 1], spins[:, 0], spins[:, 1], spins[:, 2],
               scale_units="inches", cmap="jet",
               scale=5, width=0.015, pivot="mid")
    cbar = plt.colorbar()
    cbar.ax.set_ylabel(r'$S_z$', rotation=0)
    plt.clim(-1, 1)

    plt.savefig(name + '.png', bbox_inches='tight', pad_inches=.1)


def read_from_txt(name, n_atoms):

    spins = np.zeros(shape=(n_atoms, 3))
    f = open(name + '.txt', 'r')
    i = 0
    for l in f:
        x = l.rstrip('\n')
        x = x.split()
        spins[i] = np.array([float(x[0]), float(x[1]), float(x[2])])
        i += 1
    f.close()

    return spins